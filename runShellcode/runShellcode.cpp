#include <Windows.h>
#include <string>

using namespace std;

void logError(char* msg);
void usage();

int main(int argc, char** argv)
{

	string inputFile = "";
	bool debug = false;
	bool onHeap = true;
	int alignment = 0;

	for (int i = 0; i < argc; i++) {
		if ((!strcmp(argv[i],"-i") || !strcmp(argv[i],"--input")) && argc > i) {
			inputFile = argv[i + 1];
		}

		if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "--break")) {
			debug = true;
		}

		if ((!strcmp(argv[i], "-m") || !strcmp(argv[i], "--memory")) && argc > i) {
			string zone = argv[i + 1];
			if (zone == "s" || zone == "stack") {
				onHeap = false;
			}

			if (zone == "h" || zone == "heap") {
				onHeap = true;
			}	
		}

		if ((!strcmp(argv[i], "-a") || !strcmp(argv[i], "--alignment")) && argc > i) {
			alignment = atoi(argv[i + 1]) % 4;
			
		}
	}

	if (inputFile.empty()) {
		usage();
	}

	HANDLE hFile = CreateFileA(inputFile.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		logError("CreateFile");
	}

	LARGE_INTEGER nLargeInteger = { 0 };
	if (!GetFileSizeEx(hFile, &nLargeInteger)) {
		logError("GetFileSizeEx");
	}

	DWORD size = nLargeInteger.LowPart + 1;
	printf("Shellcode size: 0x%x\n", size - 1);
	if (debug) {
		printf("Software breakpoint enabled\n");
	}
	else {
		printf("Software breakpoint disabled\n");
	}
	if (onHeap) {
		printf("Allocation method: Heap\n");
	}
	else {
		printf("Allocation method: Stack\n");
	}
	
	HANDLE hBuff = HeapCreate(0, size, size + 1);
	unsigned char* buff = (unsigned char*)HeapAlloc(hBuff, 0, size);
	DWORD oldProtection = 0;
	if (!VirtualProtect(buff, size, PAGE_EXECUTE_READWRITE, &oldProtection)) {
		logError("VirtualProtect");
	}

	DWORD read = 0;

	if (debug) {
		buff[0] = '\xcc';
	}
	else {
		buff[0] = '\x90';
	}

	if (!ReadFile(hFile, buff + 1, size - 1, &read, NULL)) {
		logError("ReadFile");
	}

	if (onHeap) {
		
		__asm {
			mov eax, buff
			and esp ,0xfffffff3
			add esp, alignment
			jmp eax
		}
	}

	else {

		int topStack = 0;
		__asm {
			mov topStack, esp
		}

		topStack -= size;
		while (topStack % 4 != 0) {
			topStack--;
		}

		__asm {
			mov esp, topStack
			sub esp, 0x100
			add esp, alignment
		}

		memcpy((char*)topStack, buff, size);

		__asm {
			mov eax,topStack
			jmp eax

		}

		CopyMemory((char*)topStack, buff, size);

		__asm {
			mov eax, topStack;
			jmp eax;
		}
	}
	
    return 1;
}

void logError(char* msg) {
	printf("[ERROR 0x%x] %s", GetLastError(), msg);
	ExitProcess(0);
}

void usage() {
	printf("\nUsage: runShellcode < -i | --input <Raw_Shellcode>> [Options]\n");
	printf("Options:\n");
	printf("    -b | --break                    Add a software breackpoint (opcode 'int 3' (0xCC))\n");
	printf("                                    before the first instruction of the shellcode.\n");
	printf("                                    This method will crash the process if no debugger are attached.\n");
	printf("    -m | --memory <h|heap|s|stack>  Area where the shellcode will be executed\n");
	printf("                   h|heap (default) Force to execute the shellcode in the heap area,\n");
	printf("                                    allocated before with RWX\n");
	printf("                   s|stack          Force to execute the shellcode in the stack area,\n");
	printf("                                    (DEP is disabled for this binary)\n");
	printf("    -a | --alignment <0..3>         ESP alignment\n\n");
	ExitProcess(0);
}

