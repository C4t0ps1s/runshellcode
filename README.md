
Run a 32 bits shellcode from a file.
Last standalone executable is accessible [here](https://bitbucket.org/C4t0ps1s/runshellcode/raw/0073a5cec24d7804064533dd68b97c20697d0fbe/Standalone/runShellcode.exe)

## Options ##
```
Usage: runShellcode < -i | --input <Raw_Shellcode>> [Options]
Options:
    -b | --break                    Add a software breackpoint (opcode 'int 3' (0xCC))
                                    before the first instruction of the shellcode.
                                    This method will crash the process if no debugger are attached.
    -m | --memory <h|heap|s|stack>  Area where the shellcode will be executed.
                   h|heap (default) Force to execute the shellcode in the heap area,
                                    allocated before with RWX
                   s|stack          Force to execute the shellcode in the stack area,
                                    (DEP is disabled for this binary)
    -a | --alignment <0..3>         ESP alignment
```

## Examples ##

Generate a calc shellcode with msfvenom:
```
msfvenom -p windows/exec -a x86 -f raw CMD="calc.exe" > shellcode_calc.raw
```

Run the shellcode from the heap, into a debugger:
```
runShellcode.exe --input shellcode_calc.raw --memory heap --break
OR
runShellcode.exe -i shellcode_calc.raw -m h -b
```

Run the same shellcode from the stack, without breaking on first instruction and with a bad alignment (ESP == 0x???????1, 0x???????5, 0x???????9 or 0x???????d):
```
runShellcode.exe --input shellcode_calc.raw --memory stack --alignment 1
OR
runShellcode.exe -i shellcode_calc.raw -m s -a 1
```